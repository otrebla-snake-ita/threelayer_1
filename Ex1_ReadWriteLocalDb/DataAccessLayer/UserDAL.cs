﻿using System;
using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public class UserDAL : IUserDAL<User>
    {

        private Three_LayerContext _context;

        public UserDAL(Three_LayerContext context)
        {
            this._context = context;
        }

        public void DeleteUser(int id)
        {
            User userDel = _context.Users.FirstOrDefault(u => u.Id == id);
            _context.Remove(userDel);
            SaveChanges();
        }

        public User GetUser(int id)
        {
            return _context.Users.FirstOrDefault(u => u.Id == id);
        }

        public List<User> GetUsers()
        {
            return _context.Users.AsNoTracking().ToList();
        }

        public void PutUser(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            
            SaveChanges();

            _context.ChangeTracker.Clear();            
        }

        public void SetUser(User user)
        {
            _context.Users.Add(user);         
            SaveChanges();
        }

        private void SaveChanges()
        {
            try
            {
                this._context.SaveChanges();
            }
            catch (Exception)
            {
                return;
            }
        }
    }
}
