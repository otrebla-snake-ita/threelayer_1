﻿using System.Collections.Generic;

namespace DataAccessLayer
{
    public interface IUserDAL<T>
    {
        public List<T> GetUsers();
        public T GetUser(int id);
        public void SetUser(T user);
        public void PutUser(T user);
        public void DeleteUser(int id);
    }
}
