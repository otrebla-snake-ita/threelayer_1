﻿using DataAccessLayer;
using DataAccessLayer.Models;
using System.Collections.Generic;

namespace BusinessLayer
{
    public class UserBL : IUserBL<User>
    {
        private IUserDAL<User> _userDal;

        public UserBL(IUserDAL<User> _userDal)
        {
            this._userDal = _userDal;
        }

        public void DeleteUser(int id)
        {
            _userDal.DeleteUser(id);
        }

        public User GetUser(int id)
        {
            return _userDal.GetUser(id);
        }

        public List<User> GetUsers()
        {
            return _userDal.GetUsers();
        }

        public void PutUser(User user)
        {
            _userDal.PutUser(user);
        }

        public void SetUser(User user)
        {
            _userDal.SetUser(user);
        }
    }
}
