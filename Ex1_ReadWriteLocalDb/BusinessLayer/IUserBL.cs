﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IUserBL<T>
    {
        public List<T> GetUsers();
        public T GetUser(int id);
        public void SetUser(T user);
        public void PutUser(T user);
        public void DeleteUser(int id);
    }
}
