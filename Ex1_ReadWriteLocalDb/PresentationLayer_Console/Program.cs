﻿using BusinessLayer;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.Extensions.DependencyInjection;
using PresentationLayer;

namespace PresentationLayer_Console
{
    class Program
    {

        static void Main()
        {
            var serviceProvider = new ServiceCollection().
                AddDbContext<Three_LayerContext>().
                AddSingleton<IUserBL<User>, UserBL>().
                AddSingleton<IUserDAL<User>, UserDAL>().
                AddSingleton<IMain, Main>().
                BuildServiceProvider();


            var main = serviceProvider.GetService<IMain>();
            main.Start();
           

        }
    }
}
