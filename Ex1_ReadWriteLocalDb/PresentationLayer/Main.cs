﻿using BusinessLayer;
using DataAccessLayer.Models;
using System;

namespace PresentationLayer
{
    public class Main : IMain
    {
        private readonly IUserBL<User> _userBL;

        public Main(IUserBL<User> userBL)
        {
            this._userBL = userBL;
        }

        public void Start()
        {


            Console.WriteLine("Premi:\n" +
                "1. visualizza tutti user\n" +
                "2. visualizza user\n" +
                "3. inserisci user\n" +
                "4. modifica user\n" +
                "5. elimina user\n" +
                "0. uscire");

            while (true)
            {
                var scelta = Console.ReadLine();
                int id;
                switch (scelta)
                {
                    case "1":
                        GetUsers(_userBL);
                        break;
                    case "2":
                        Console.WriteLine("inserisci id User");
                        id = Int32.Parse(Console.ReadLine());
                        GetUser(id, _userBL);
                        break;
                    case "3":
                        Console.WriteLine("Creazione dell'utente completata.");
                        SetUser(_userBL);
                        break;
                    case "4":
                        Console.WriteLine("inserisci id User");
                        id = Int32.Parse(Console.ReadLine());
                        PutUser(_userBL, id);
                        break;
                    case "5":
                        Console.WriteLine("inserisci id User");
                        int idDelete = Int32.Parse(Console.ReadLine());
                        DeleteUser(idDelete, _userBL);
                        break;
                    case "0":
                        return;
                }
            }

        }

        public static void PutUser(IUserBL<User> _bl, int id)
        {
            _bl.PutUser(new User
            {     
                Id = id,
                Name = "Mod",
                Surname = "Mod",
                Age = 25,
                Email = "Mod",
                Username = "Mod"
            });
        }

        public static void DeleteUser(int id, IUserBL<User> _bl)
        {
            _bl.DeleteUser(id);
        }

        public static void GetUser(int id, IUserBL<User> _bl)
        {
            var user = _bl.GetUser(id);
            Console.WriteLine(user.Name + " " + user.Surname + " " + user.Age + " " + user.Email + " " + user.Username);

        }

        public static void SetUser(IUserBL<User> _bl)
        {
            _bl.SetUser(new User
            {
                Name = "Prova",
                Surname = "Prova",
                Age = 25,
                Email = "Prova",
                Username = "Prova"
            });
        }

        public static void GetUsers(IUserBL<User> _bl)
        {
            var listaUser = _bl.GetUsers();

            foreach (var ele in listaUser)
            {
                Console.WriteLine(ele.Name + " " + ele.Surname + " " + ele.Age + " " + ele.Email + " " + ele.Username);
            }
        }

    }
}
